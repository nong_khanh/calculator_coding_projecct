var getInputView = document.querySelector('.calculator__top--view');
var getInputResult = document.querySelector('.calculator__top--result');
var getBtnNumber = document.querySelectorAll('.calculator__items--number');
var getBtnOperator = document.querySelectorAll('.calculator__items--operator');
var getBtnDelete = document.querySelectorAll('.calculator__items--delete');
var getBtn = document.querySelectorAll('.btn');

// function
function getValues(collection, cb) { // cb se tra ve array value cua nhung button dc click
    var emtyArr = [];
    for(let i = 0; i < collection.length; i++) {
        collection[i].onclick = function(e) {
            emtyArr.push(e.target.getAttribute('value'));
            return cb(emtyArr);
        }
    }
}

function convertToString(array) {
    var string = array.join('');
    return string;
}

// logic code
var getResult = getValues(getBtn, function(array) { 
    getInputView.value = convertToString(array);
    var resultArray = [];
    if(array[array.length - 1] === '=') {
        array.pop();
        getInputView.value = convertToString(array);       
        var result = eval(convertToString(array));  
        resultArray.push(result);
        getInputResult.value = result;    
        sessionStorage.setItem("result", result);
    } 
    if(array[array.length - 1] === "DEL") {
        array.pop();
        array.pop();
        let copyArr = array;
        getInputView.value = convertToString(copyArr); 
        getInputResult.value = '';
    };
    if(array[array.length - 1] === "AC") {
        array.splice(0, array.length);
        let copyArr = array;
        getInputView.value = convertToString(copyArr); 
        getInputResult.value = '';
    };
    if(sessionStorage.getItem('result') == getInputResult.value && array[array.length - 1] === '+') {     
        array.splice(0, array.length, sessionStorage.getItem('result') + '+');
        getInputView.value = convertToString(array);   
        getInputResult.value = '';  
    }   
    if(sessionStorage.getItem('result') == getInputResult.value && array[array.length - 1] === '-') {     
        array.splice(0, array.length, sessionStorage.getItem('result') + '-');
        getInputView.value = convertToString(array);   
        getInputResult.value = '';  
    }
    if(sessionStorage.getItem('result') == getInputResult.value && array[array.length - 1] === '*') {     
        array.splice(0, array.length, sessionStorage.getItem('result') + '*');
        getInputView.value = convertToString(array);   
        getInputResult.value = '';  
    }
    if(sessionStorage.getItem('result') == getInputResult.value && array[array.length - 1] === '/') {     
        array.splice(0, array.length, sessionStorage.getItem('result') + '/');
        getInputView.value = convertToString(array);   
        getInputResult.value = '';  
    }


});


